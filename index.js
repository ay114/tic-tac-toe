//set up the game
let center = document.getElementsByClassName('center');
let gridItems = document.getElementsByClassName('grid-item');
let restart = document.getElementsByClassName('restart');
let turnNumber = 9;
let winner;
let x = [];
let o = [];

//set winning condition
// let winningCondition = ["123", "456", "789", "147", "258", "369", "159", "357"];
let winning = {
    "1": { "2": "3", "4": "7", "5": "9" },
    "2": { "5": "8" },
    "3": { "6": "9", "5": "7" },
    "4": { "5": "6" },
    "7": { "8": "9" }
}

//set up current player info
let currentPlayer = document.createElement('p');
let player;
updateCurrentPlayer();
center[0].appendChild(currentPlayer);

//to update curent player display
function updateCurrentPlayer() {
    player = (turnNumber % 2) === 1 ? "X" : "O";
    currentPlayer.textContent = player + '\'s turn';
}

// 1359, 359
// 246, 246
//recursive haha
function checkVictory(names, win, index) {
    // console.log(names, win)
    if (typeof win === "string") { return names.includes(win); }
    if (!win || win.length < 1) { return false; }
    if (index >= names.length) { return false; }

    let keys = Object.keys(win);
    if (keys.includes(names[index])) {
        return checkVictory(names.slice(1, names.length), win[names[index]], 0);
    } else {
        return checkVictory(names, win, index + 1);
    }

}

//display x and o
for (let i = 0; i < gridItems.length; i++) {
    gridItems[i].addEventListener('click', () => {
        if (!gridItems[i].textContent) {
            let id = gridItems[i].id;
            if (player === "X") {
                x.push(id);
                if (x.length >= 3) {
                    // console.log("x is: ", x);
                    // console.log("o is: ", o);
                    if (checkVictory(x, winning, 0)) {
                        winner = "X";
                        endGame(winner + ' won!');
                    }
                }
            } else {
                o.push(id);
                if (o.length >= 3) {
                    if (checkVictory(o, winning, 0)) {
                        winner = "O";
                        endGame(winner + ' won!');
                    }
                }
            }
            gridItems[i].textContent = player;

            //update if no winner
            if (!winner) {
                turnNumber--;
                updateCurrentPlayer();
            }
            //in case of a tie
            if (x.length + o.length === 9) {
                endGame("Tie!")
            }
        }
    })
};

//end the game
function endGame(message) {
    currentPlayer.textContent = message;
    for (let i = 0; i < gridItems.length; i++) {
        gridItems[i].textContent = gridItems[i].textContent ? gridItems[i].textContent : " ";
    }
}

//restart the game and clear stats
restart[0].addEventListener('click', () => {
    for (let i = 0; i < gridItems.length; i++) {
        turnNumber = 9;
        gridItems[i].textContent = '';
        updateCurrentPlayer();
        winner = null;
        x = [];
        o = [];
    }
})